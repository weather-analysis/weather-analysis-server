package org.alvis.service;

import org.alvis.service.impl.CollectWeatherServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: 数据收集测试
 * @author: luchengwen
 * @create: 2020-08-09 14:29
 **/
@SpringBootTest
public class WeatherCollectTest {

    @Autowired
    private ICollectWeatherService service;



    @Test
    private void collectTest() throws IOException {
        List<String> list = Arrays.asList("昆明市", "杭州市");
        service.collectWeather(list);
    }
}
