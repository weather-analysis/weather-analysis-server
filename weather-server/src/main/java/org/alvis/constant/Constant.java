package org.alvis.constant;

/**
 * @program: weather-analysis
 * @description: 天气服务常用的常量
 * @author: luchengwen
 * @create: 2020-08-09 13:56
 **/

public class Constant {

    public static final String WEATHER_REQUEST_URI = "http://wthrcdn.etouch.cn/weather_mini?";

    public static final long TIME_OUT = 2000L;
}
