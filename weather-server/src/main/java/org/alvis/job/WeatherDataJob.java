package org.alvis.job;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.alvis.common.ServerResponse;
import org.alvis.service.ICollectWeatherService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

import static com.sun.xml.internal.ws.api.message.Packet.State.ServerResponse;

/**
 * @program: weather-analysis
 * @description: 获取天气定时器
 * @author: luchengwen
 * @create: 2020-08-09 17:23
 **/
@Slf4j
@Component
public class WeatherDataJob {


    @Autowired
    private ICollectWeatherService service;

    @Autowired
    private RestTemplate restTemplate;


    //    @Scheduled(cron="*/10 * * * * ?") 十分钟跑一次数据
    @Scheduled(cron = "*/60 * * * * ?")
    private void process() throws IOException {
        log.info("Weather Data Sync Job. Start！");
        // 获取城市ID列表
        String url = "http://city-server:8800/city/queryDistrict";
        List<String> cityList = restTemplate.getForObject(url, List.class);
        service.collectWeather(cityList);
        log.info("Weather Data Sync Job. End！");
    }

}
