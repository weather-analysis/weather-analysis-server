package org.alvis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: weather-analysis
 * @description: 天气数据
 * @author: luchengwen
 * @create: 2020-08-09 15:59
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Weather implements Serializable {

    private static final long serialVersionUID = 6531149463632986139L;

    /**
     * 时间
     */
    private String date;

    /**
     * 最高温度
     */
    private String high;
    /**
     * 风力
     */
    private String fengli;

    /**
     * 最低温度
     */
    private String low;

    /**
     * 风向
     */
    private String fengxiang;

    /**
     * 类型
     */
    private String type;


}
