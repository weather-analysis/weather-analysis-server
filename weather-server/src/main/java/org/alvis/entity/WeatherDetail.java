package org.alvis.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: 详细的天气数据
 * @author: luchengwen
 * @create: 2020-08-09 16:11
 **/

public class WeatherDetail implements Serializable {
    private static final long serialVersionUID = 5265333165862043655L;

    /**
     * 城市
     */
    private String city;

    /**
     * 昨天的天气信息
     */
    private Weather yesterday;

    /**
     * 未来天气信息
     */
    private List<Weather> forecast;

    /**
     * 感冒
     */
    private String ganmao;

    /**
     * 温度
     */
    private String wendu;
}
