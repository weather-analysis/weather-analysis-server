package org.alvis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: weather-analysis
 * @description: 天气信息数据保存到数据中的设计
 * @author: luchengwen
 * @create: 2020-08-09 16:33
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherContentInfo implements Serializable {


    private static final long serialVersionUID = -3571841788878722585L;

    /**
     * 城市
     */
    private String city;

    /**
     * 保存的天气数据
     */
    private String weatherTime;

    /**
     * 插入时间
     */
    private String creatTime;


    /**
     * 内容
     */
    private String content;

    /**
     * 标识位 <p>1有效的</p>
     * <p>0 已删除</p>
     */
    private int flag;

    /**
     * 保存的数据类型
     */
    private int type;
}
