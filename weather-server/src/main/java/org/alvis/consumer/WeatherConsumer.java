package org.alvis.consumer;

import com.google.gson.Gson;
import org.alvis.dao.IWeatherDao;
import org.alvis.entity.WeatherContentInfo;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: weather-analysis
 * @description: 把mq的数据存到数据库中
 * @author: luchengwen
 * @create: 2020-08-09 17:05
 **/
@Component
public class WeatherConsumer {

    @Autowired
    private IWeatherDao weatherDao;

    @RabbitListener(bindings =
    @QueueBinding(
            value = @Queue,
            exchange =
            @Exchange(name = "weather_info", type = "fanout")
    ))
    public void consumer(String message) {
        WeatherContentInfo info = new Gson().fromJson(message, WeatherContentInfo.class);
        weatherDao.insertWeather(info);
    }
}
