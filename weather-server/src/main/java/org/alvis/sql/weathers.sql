-- ----------------------------
-- Table structure for `weathers`
-- ----------------------------
DROP TABLE
IF
	EXISTS `weathers`;
CREATE TABLE `weathers` (
	`city` VARCHAR ( 32 ) NOT NULL DEFAULT "" COMMENT "城市",
	`weather_time` VARCHAR ( 64 ) NOT NULL DEFAULT "" COMMENT "保存的天气数据",
	`creatTime` VARCHAR ( 64 ) NOT NULL DEFAULT "" COMMENT "插入时间",
	`content` VARCHAR ( 1024 ) NOT NULL DEFAULT "" COMMENT "内容",
	`flag` TINYINT ( 8 ) NOT NULL DEFAULT 1 COMMENT "标识位",
`type` TINYINT ( 8 ) NOT NULL DEFAULT 1 COMMENT "类型"
) ENGINE = INNODB DEFAULT CHARSET = utf8;