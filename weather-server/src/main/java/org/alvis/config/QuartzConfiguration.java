package org.alvis.config;

import org.springframework.context.annotation.Configuration;

/**
 * @program: weather-analysis
 * @description: 定时器配置
 * @author: luchengwen
 * @create: 2020-08-09 17:22
 **/
@Configuration
public class QuartzConfiguration {


//    private static final int TIME = 60; // 更新频率
//
//    // JobDetail
//    @Bean
//    public JobDetail weatherDataSyncJobDetail() {
//        return JobBuilder.newJob(WeatherDataSyncJob.class).withIdentity("weatherDataSyncJob")
//                .storeDurably().build();
//    }
//
//    // Trigger
//    @Bean
//    public Trigger weatherDataSyncTrigger() {
//
//        Trigger trigger = TriggerBuilder
//                .newTrigger()
//                .withIdentity("weatherDataSyncJob", "group1")
//                .withSchedule(
//                        CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
//                .build();
//        return trigger;
//    }
}
