package org.alvis.dao;

import org.alvis.entity.WeatherContentInfo;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: weather的dao层接口
 * @author: luchengwen
 * @create: 2020-08-09 19:06
 **/

public interface IWeatherDao {

    boolean insertWeather(WeatherContentInfo info);

    List<WeatherContentInfo> findAll();
}
