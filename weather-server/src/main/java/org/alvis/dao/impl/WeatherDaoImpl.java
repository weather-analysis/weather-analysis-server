package org.alvis.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.alvis.dao.IWeatherDao;
import org.alvis.entity.WeatherContentInfo;
import org.alvis.helper.WeatherSQLHelper;
import org.alvis.rowMapper.WeatherContentInfoRowMapper;
import org.alvis.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: IWeatherDao实现类
 * @author: luchengwen
 * @create: 2020-08-09 19:07
 **/

@Repository
@Slf4j
public class WeatherDaoImpl implements IWeatherDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean insertWeather(WeatherContentInfo info) {

        Object[] args = WeatherSQLHelper.getArgs(info);
        log.info("插入天气数据数据，city:[{}][{}]", info.getCity(), info.getWeatherTime());
        StringBuffer sql = new StringBuffer("INSERT INTO weathers ");
        sql.append("(city, content, type, flag, creatTime,weather_time) VALUES ");
        sql.append("(?, ?, ?, ?, ?, ?) ");
        int affectNum = jdbcTemplate.update(sql.toString(), args);
        if (affectNum > 0) {
            log.info("插入成功,，city:[{}][{}]", info.getCity(), info.getWeatherTime());
            return true;
        }
        log.error("插入失败,，city:[{}][{}]", info.getCity(), info.getWeatherTime());
        return false;
    }

    public List<WeatherContentInfo> findAll() {
        StringBuffer buffer = new StringBuffer();
        Object[] args = new Object[]{DateUtils.getNow()};
        buffer.append("SELECT * FROM weathers where creatTime=? and flag=1");
        List<WeatherContentInfo> infoList = jdbcTemplate.query(buffer.toString(), args, new WeatherContentInfoRowMapper());
        return infoList;
    }
}
