package org.alvis.rowMapper;

import org.alvis.entity.WeatherContentInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @program: weather-analysis
 * @description: WeatherContentInfo相关的mapper
 * @author: luchengwen
 * @create: 2020-11-24 14:30
 **/

public class WeatherContentInfoRowMapper implements RowMapper<WeatherContentInfo> {
    @Override
    public WeatherContentInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        WeatherContentInfo info = new WeatherContentInfo();
        info.setCity(resultSet.getString("city"));
        info.setCreatTime(resultSet.getString("creatTime"));
        info.setWeatherTime(resultSet.getString("weather_time"));
        info.setContent(resultSet.getString("content"));
        info.setType(resultSet.getInt("type"));
        info.setFlag(resultSet.getInt("flag"));
        return info;
    }
}
