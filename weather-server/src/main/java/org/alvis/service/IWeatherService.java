package org.alvis.service;

import org.alvis.entity.WeatherContentInfo;

import java.util.List;

/**
 * @program: weather-analysis
 * @description:
 * @author: luchengwen
 * @create: 2020-11-24 14:21
 **/
public interface IWeatherService {

    List<WeatherContentInfo> getWeatherInfo();
}
