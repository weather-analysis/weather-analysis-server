package org.alvis.service.impl;

import org.alvis.dao.IWeatherDao;
import org.alvis.entity.WeatherContentInfo;
import org.alvis.service.IWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: weather相关server
 * @author: luchengwen
 * @create: 2020-11-24 14:20
 **/

@Service
public class WeatherServiceImpl implements IWeatherService {

    @Autowired
    private IWeatherDao weatherDao;

    public List<WeatherContentInfo> getWeatherInfo() {
        List<WeatherContentInfo> all = weatherDao.findAll();
        return all;
    }
}
