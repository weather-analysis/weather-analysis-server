package org.alvis.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.alvis.common.CommonConstant;
import org.alvis.common.ServerResponse;
import org.alvis.constant.Constant;
import org.alvis.entity.WeatherContentInfo;
import org.alvis.service.ICollectWeatherService;
import org.alvis.utils.DateUtils;
import org.alvis.utils.StringUtils;
import org.alvis.utils.ValidUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: ICollectWeatherService的实现
 * @author: luchengwen
 * @create: 2020-08-09 14:15
 **/

@Service
public class CollectWeatherServiceImpl implements ICollectWeatherService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void collectWeather(List<String> cities) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        if (ValidUtils.isEmpty(cities))
            return;
        for (String city : cities) {
            String url = Constant.WEATHER_REQUEST_URI + "city=" + city;
            ResponseEntity<String> responseResult = restTemplate.getForEntity(url, String.class);
            if (responseResult.getStatusCodeValue() == 200) {
                String result = responseResult.getBody();
                String data = StringUtils.conventFromGzip(result);
                WeatherContentInfo info = new WeatherContentInfo();
                info.setCity(city);
                info.setContent(data);
                info.setWeatherTime(DateUtils.getLocalDateNow());
                info.setType(CommonConstant.WEATHER_INFO_TYPE);
                info.setFlag(CommonConstant.IS_FLAG);
                info.setCreatTime(DateUtils.getNow());
                String content = new Gson().toJson(info);
                // 拿到数据后往rabbitmq放(fanout 模式)
                rabbitTemplate.convertAndSend("weather_info", "", content);
            }
        }

    }

    @Override
    public String getCityWeatherInfo(String cityName) throws IOException {
        if (ValidUtils.isEmpty(cityName))
            return null;
        String url = Constant.WEATHER_REQUEST_URI + "city=" + cityName;
        ResponseEntity<String> responseResult = restTemplate.getForEntity(url, String.class);
        String data = "";
        if (responseResult.getStatusCodeValue() == 200) {
            String result = responseResult.getBody();
            data = StringUtils.conventFromGzip(result);
        }
        return ValidUtils.isEmpty(data) == true ? "" : data;
    }


}