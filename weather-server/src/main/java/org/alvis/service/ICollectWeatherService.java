package org.alvis.service;

import java.io.IOException;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: 收集天气的service
 * @author: luchengwen
 * @create: 2020-08-09 14:12
 **/
public interface ICollectWeatherService {

    /**
     * 功能描述 把所有的城市天气数据查出来
     *
     * @param cities 查询的城市列表
     * @return null
     */
    void collectWeather(List<String> cities) throws IOException;

    String getCityWeatherInfo(String cityName) throws IOException;
}
