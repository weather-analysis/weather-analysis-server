package org.alvis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @program: weather-analysis
 * @description: 天气服务相关启动类
 * @author: luchengwen
 * @create: 2020-08-09 13:48
 **/

@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
public class WeatherServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherServerApplication.class, args);
    }
}
