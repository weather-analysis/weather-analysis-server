package org.alvis.controller;

import org.alvis.entity.Weather;
import org.alvis.entity.WeatherContentInfo;
import org.alvis.service.IWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: 天气相关服务
 * @author: luchengwen
 * @create: 2020-11-24 14:09
 **/
@RequestMapping("/weather")
@RestController
public class WeatherController {

    @Autowired
    private IWeatherService weatherService;

    @RequestMapping("/getAll")
    public List<WeatherContentInfo> getAll() {
        List<WeatherContentInfo> weatherInfo = weatherService.getWeatherInfo();
        return weatherInfo;
    }
}
