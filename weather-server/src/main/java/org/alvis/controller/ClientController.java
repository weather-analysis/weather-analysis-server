package org.alvis.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @program: weather-analysis
 * @description: client
 * @author: luchengwen
 * @create: 2020-11-24 15:04
 **/

@RestController
public class ClientController {

    @RequestMapping("/getCityName")
    public String getCity() {
        RestTemplate template = new RestTemplate();
        String object = template.getForObject("http://127.0.0.1:8800/city/getCityName", String.class);
        return object;
    }
}
