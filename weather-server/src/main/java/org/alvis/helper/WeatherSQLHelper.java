package org.alvis.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.alvis.entity.WeatherContentInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: 用于生成weather插入的工具类
 * @author: luchengwen
 * @create: 2020-08-09 19:02
 **/

public class WeatherSQLHelper {

    public static Object[] getArgs(WeatherContentInfo info) {
        List<Object> res = new ArrayList<>();
        res.add(info.getCity());
        res.add(info.getContent());
        res.add(info.getType());
        res.add(info.getFlag());
        res.add(info.getCreatTime());
        res.add(info.getWeatherTime());
        return res.toArray();
    }
}
