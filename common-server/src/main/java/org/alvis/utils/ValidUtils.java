package org.alvis.utils;

import java.util.Collection;

/**
 * @program: weather-analysis
 * @description: 校验的数组
 * @author: luchengwen
 * @create: 2020-08-08 17:19
 **/

public class ValidUtils {

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(String obj) {
        return "".equals(obj) == true || obj.isEmpty();
    }
}
