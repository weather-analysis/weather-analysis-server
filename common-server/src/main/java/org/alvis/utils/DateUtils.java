package org.alvis.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @program: weather-analysis
 * @description: 时间类型处理
 * @author: luchengwen
 * @create: 2020-08-09 16:42
 **/

public class DateUtils {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH");

    private static final DateTimeFormatter LOCALDATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static String getNow() {
        LocalDateTime now = LocalDateTime.now();
        String nowStr = FORMATTER.format(now);
        return nowStr;
    }

    public static String getLocalDateNow() {
        LocalDate date = LocalDate.now();
        String nowStr = LOCALDATE_FORMATTER.format(date);
        return nowStr;
    }

}
