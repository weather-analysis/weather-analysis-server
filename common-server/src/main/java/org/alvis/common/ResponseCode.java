package org.alvis.common;



public enum ResponseCode {
    SUCCESS(200, "响应成功！"),
    ERROR(500, "响应失败"),
    INSERT_ERROR(10001,"添加失败");

    private int code;
    private String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
