package org.alvis.common;

/**
 * @program: weather-analysis
 * @description: 常用的常量
 * @author: luchengwen
 * @create: 2020-08-09 16:38
 **/

public class CommonConstant {

    /**
     * 保存的天气的类型
     */
    public static final int WEATHER_INFO_TYPE = 0;

    /**
     * 是否有效
     */
    public static final int IS_FLAG = 1;

    /**
     * 是否有效
     */
    public static final int IS_NOT_FLAG = 0;


}
