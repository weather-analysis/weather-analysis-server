package org.alvis.controller;

import org.alvis.common.ResponseCode;
import org.alvis.common.ServerResponse;
import org.alvis.entity.City;
import org.alvis.service.ICityService;
import org.alvis.utils.ValidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: 对外提供的cotroller
 * @author: luchengwen
 * @create: 2020-08-08 17:55
 **/

@RestController()
public class CityController {

    @Autowired
    private ICityService cityService;

    @GetMapping("/getCity")
    public ServerResponse<List<City>> getCityList() {
        List<City> cityList = cityService.findAll();
        if (ValidUtils.isEmpty(cityList))
            ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), "数据为空");
        ServerResponse<List<City>> response = ServerResponse.createBySuccess(cityList);
        return response;
    }

    @GetMapping("/queryByCity")
    public ServerResponse<List<City>> queryByCity(@RequestParam("city") String city) {
        List<City> cityList = cityService.findByCity(city);
        if (ValidUtils.isEmpty(cityList))
            ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), "数据为空");
        ServerResponse<List<City>> response = ServerResponse.createBySuccess(cityList);
        return response;
    }

    @PostMapping("/add")
    public ServerResponse<Boolean> queryByCity(City city) {
        boolean isSuccess = cityService.add(city);
        if (!isSuccess)
            ServerResponse.createByErrorCodeMessage(ResponseCode.INSERT_ERROR.getCode(), ResponseCode.INSERT_ERROR.getDesc());
        ServerResponse<Boolean> success = ServerResponse.createBySuccess(isSuccess, "添加成功");
        return success;
    }

    @GetMapping("/queryDistrict")
    public List<String> getDistrict() {
        List<String> cityList = cityService.findByDistrict();
        if (ValidUtils.isEmpty(cityList))
            ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), "数据为空");
        return cityList;
    }

    @GetMapping("/getDistrictByCity")
    public List<String> getDistrictByCity(@RequestParam("city") String city) {
        List<String> cityList = cityService.findByDistrictByDistrict(city);
        if (ValidUtils.isEmpty(cityList))
            ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), "数据为空");
        return cityList;
    }

    @GetMapping("/getCityName")
    public List<String> queryCityNameAll() {
        List<String> citys = cityService.queryCityNameAll();
        return citys;
    }
}
