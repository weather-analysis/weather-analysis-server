-- ----------------------------
-- Table structure for `citys`
-- ----------------------------
DROP TABLE
IF
	EXISTS `citys`;
CREATE TABLE `citys` (
	`city` VARCHAR ( 128 ) NOT NULL DEFAULT "" COMMENT "城市",
	`cityName` VARCHAR ( 64 ) NOT NULL DEFAULT "" COMMENT "城市名",
	`district` VARCHAR ( 32 ) NOT NULL DEFAULT "" COMMENT "区县",
	`longitude` VARCHAR ( 32 ) NOT NULL DEFAULT "" COMMENT "经度",
`latitude` VARCHAR ( 255 ) NOT NULL DEFAULT "" COMMENT "纬度"
) ENGINE = INNODB DEFAULT CHARSET = utf8;