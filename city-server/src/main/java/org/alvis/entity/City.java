package org.alvis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: weather-analysis
 * @description: 全国的城市信息PO
 * @author: luchengwen
 * @create: 2020-08-08 13:51
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
public class City implements Serializable {


    private static final long serialVersionUID = -6187093522816584591L;
    /**
     * 城市
     */
    private String city;

    /**
     * 地市
     */
    private String cityName;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     * 区县
     */
    private String district;

    public static City initCity() {
        return new City();
    }
}
