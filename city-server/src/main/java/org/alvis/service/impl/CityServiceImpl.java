package org.alvis.service.impl;

import org.alvis.dao.ICityDao;
import org.alvis.entity.City;
import org.alvis.service.ICityService;
import org.alvis.utils.ValidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: city具体实现
 * @author: luchengwen
 * @create: 2020-08-08 17:30
 **/

@Service
public class CityServiceImpl implements ICityService {

    @Autowired
    private ICityDao cityDao;

    @Override
    public List<City> findAll() {
        List<City> cities = cityDao.findAll();
        if (ValidUtils.isEmpty(cities)) {
            cityDao.insertBasicData();
        }
        return cities;
    }

    @Override
    public List<City> findByCity(String city) {
        List<City> cities = cityDao.findByCity(city);
        if (ValidUtils.isEmpty(cities)) {
            cityDao.insertBasicData();
        }
        return cities;
    }

    @Override
    public boolean add(City city) {
        boolean isSuccess = cityDao.addCity(city);
        return isSuccess == true ? true : false;
    }

    @Override
    public List<String> findByDistrict() {
        List<String> cities = cityDao.findByDistrict();
        if (ValidUtils.isEmpty(cities)) {
            cityDao.insertBasicData();
        }
        return cities;
    }

    @Override
    public List<String> findByDistrictByDistrict(String city) {
        List<String> cities = cityDao.findByDistrictByCity(city);
        if (ValidUtils.isEmpty(cities)) {
            cityDao.insertBasicData();
        }
        return cities;
    }

    public List<String> queryCityNameAll() {
        List<String> cities = cityDao.findCityNameALll();
        return cities;
    }
}
