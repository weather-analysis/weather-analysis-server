package org.alvis.service;

import org.alvis.entity.City;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: 城市server的service层接口
 * @author: luchengwen
 * @create: 2020-08-08 17:28
 **/
public interface ICityService {

    List<City> findAll();

    List<City> findByCity(String city);

    boolean add(City city);

    List<String> findByDistrict();

    List<String> findByDistrictByDistrict(String city);

    List<String> queryCityNameAll();
}
