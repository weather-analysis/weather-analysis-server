package org.alvis.rowMapper;

import org.alvis.entity.City;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @program: weather-analysis
 * @description: 城市信息RowMapper
 * @author: luchengwen
 * @create: 2020-08-08 13:57
 **/

public class CityRowMapper implements RowMapper<City> {

    @Override
    public City mapRow(ResultSet resultSet, int i) throws SQLException {
        City city = City.initCity();
        city.setCityName(resultSet.getString("cityName"));
        city.setCity(resultSet.getString("city"));
        city.setLongitude(resultSet.getString("longitude"));
        city.setLatitude(resultSet.getString("latitude"));
        city.setDistrict(resultSet.getString("district"));
        return city;
    }
}
