package org.alvis.rowMapper;

import org.springframework.jdbc.core.RowMapper;

import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @program: weather-analysis
 * @description: districtRowMapper的映射
 * @author: luchengwen
 * @create: 2020-08-08 19:01
 **/

public class CityDistrictRompper implements RowMapper<String> {
    @Override
    public String mapRow(ResultSet resultSet, int i) throws SQLException {
        String str = resultSet.getString("district");
        return str;
    }
}
