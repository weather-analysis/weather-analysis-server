package org.alvis.rowMapper;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @program: weather-analysis
 * @description:
 * @author: luchengwen
 * @create: 2020-11-24 14:02
 **/

public class CityNameRompper implements RowMapper<String> {
    @Override
    public String mapRow(ResultSet resultSet, int i) throws SQLException {
        String cityName = resultSet.getString("cityName");
        return cityName;
    }
}
