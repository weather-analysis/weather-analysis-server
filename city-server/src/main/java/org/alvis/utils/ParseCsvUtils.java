package org.alvis.utils;

import org.alvis.entity.City;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: 用于解析csv城市数据
 * @author: luchengwen
 * @create: 2020-08-08 14:25
 **/

public class ParseCsvUtils {

    public static List<City> parseCsvCity() {
        List<City> parseResult = new ArrayList<>();
        File csv = getCityResource();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(csv));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            while ((br.readLine()) != null)  //读取到的内容给line变量
            {
                String[] split = br.readLine().split(",");
                City city = City.initCity();
                city.setCity(split[0]);
                city.setCityName(split[1]);
                city.setDistrict(split[2]);
                city.setLongitude(split[3]);
                city.setLatitude(split[4]);
                parseResult.add(city);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parseResult;
    }

    public static File getCityResource(){
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String basicPath = "org.alvis.utils";
        String replaceStr = File.separator;
        basicPath = basicPath.replace(".", replaceStr);
        URL url = classLoader.getResource(basicPath);
        if (url != null) {
            String urlPath = url.getPath();
            urlPath = urlPath.substring(0,urlPath.lastIndexOf("target"));
            String separator = urlPath.substring(urlPath.length()-1,urlPath.length());
            String relativePath = "src" + separator + "main"  + separator + "resources"  + separator +  "citys.csv";

            String realPath =  urlPath + relativePath;
            File file = new File(realPath);
            return file;
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        parseCsvCity();
    }


}
