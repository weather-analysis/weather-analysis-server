package org.alvis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @program: weather-analysis
 * @description: 城市服务的入口程序
 * @author: luchengwen
 * @create: 2020-08-07 16:30
 **/

@SpringBootApplication
@EnableDiscoveryClient
public class CityServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(CityServerApplication.class, args);
    }
}
