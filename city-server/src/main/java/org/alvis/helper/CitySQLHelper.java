package org.alvis.helper;

import org.alvis.entity.City;
import org.omg.CORBA.OBJ_ADAPTER;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: weather-analysis
 * @description: 用于生成插入数据参数
 * @author: luchengwen
 * @create: 2020-08-08 16:24
 **/

public class CitySQLHelper {

    /**
     * 功能描述 生成标准SQL插入城市信息操作需要的Object数组
     *
     * @param city
     * @return Object[] args.toArray()
     */
    public static Object[] insertCityArgs(City city) {
        List<Object> args = new ArrayList<>();
        args.add(city.getLongitude());
        args.add(city.getLatitude());
        args.add(city.getCity());
        args.add(city.getCityName());
        args.add(city.getDistrict());
        Object[] result = args.toArray();
        return result;
    }
}
