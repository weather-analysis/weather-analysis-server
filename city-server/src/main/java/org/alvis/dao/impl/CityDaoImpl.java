package org.alvis.dao.impl;

import lombok.extern.slf4j.Slf4j;
import org.alvis.dao.ICityDao;
import org.alvis.entity.City;
import org.alvis.helper.CitySQLHelper;
import org.alvis.rowMapper.CityDistrictRompper;
import org.alvis.rowMapper.CityNameRompper;
import org.alvis.rowMapper.CityRowMapper;
import org.alvis.utils.ParseCsvUtils;
import org.alvis.utils.ValidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: weather-analysis
 * @description: ICityDao的实现
 * @author: luchengwen
 * @create: 2020-08-08 16:18
 **/
@Repository
@Slf4j
public class CityDaoImpl implements ICityDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void insertBasicData() {
        List<City> cities = ParseCsvUtils.parseCsvCity();
        for (City city : cities) {
            addCity(city);
        }
    }

    @Override
    public boolean addCity(City city) {
        Object[] args = CitySQLHelper.insertCityArgs(city);
        log.info("插入城市数据，city:[{}][{}][{}]", city.getCity(), city.getCityName(), city.getDistrict());
        StringBuffer sql = new StringBuffer("INSERT INTO citys ");
        sql.append("(longitude, latitude, city, cityName, district) VALUES ");
        sql.append("(?, ?, ?, ?, ?) ");
        int affectNum = jdbcTemplate.update(sql.toString(), args);
        if (affectNum > 0) {
            log.info("插入成功,city:[{}][{}][{}]", city.getCity(), city.getCityName(), city.getDistrict());
            return true;
        }
        log.error("插入失败,city:[{}][{}][{}]", city.getCity(), city.getCityName(), city.getDistrict());
        return false;
    }

    @Override
    public List<City> findAll() {
        StringBuffer sql = new StringBuffer();
        sql.append("select * from citys");
        List<City> result = jdbcTemplate.query(sql.toString(), new CityRowMapper());
        return ValidUtils.isEmpty(result) ? null : result;
    }

    @Override
    public List<City> findByCity(String city) {
        StringBuffer sql = new StringBuffer();
        List<Object> args = new ArrayList<>();
        sql.append("select * from citys where city = ? ");
        args.add(city);
        List<City> result = jdbcTemplate.query(sql.toString(), args.toArray(), new CityRowMapper());
        return ValidUtils.isEmpty(result) ? null : result;
    }

    @Override
    public List<String> findByDistrict() {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT district from citys");
        List<String> result = jdbcTemplate.query(sql.toString(), new CityDistrictRompper());
        return ValidUtils.isEmpty(result) ? null : result;
    }

    @Override
    public List<String> findByDistrictByCity(String city) {
        StringBuffer sql = new StringBuffer();
        List<Object> args = new ArrayList<>();
        sql.append("SELECT DISTINCT district from citys where city = ?");
        args.add(city);
        List<String> result = jdbcTemplate.query(sql.toString(), args.toArray(), new CityDistrictRompper());
        return ValidUtils.isEmpty(result) ? null : result;
    }

    public List<String> findCityNameALll() {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT DISTINCT cityName FROM citys");
        List<String> list = jdbcTemplate.query(sql.toString(), new CityNameRompper());
        return list;
    }
}
