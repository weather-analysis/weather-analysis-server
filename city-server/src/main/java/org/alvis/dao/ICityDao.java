package org.alvis.dao;

import org.alvis.entity.City;

import java.util.List;


/**
 * @program: weather-analysis
 * @description: city的dao层接口
 * @author: luchengwen
 * @create: 2020-08-08 16:09
 **/
public interface ICityDao {

    /**
     * 插入基础的城市数据
     */
    void insertBasicData();

    boolean addCity(City city);

    List<City> findAll();

    List<City> findByCity(String city);

    List<String> findByDistrict();

    List<String> findByDistrictByCity(String city);

    List<String> findCityNameALll();
}
