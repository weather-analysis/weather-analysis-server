package org.alvis.city.service;

import org.alvis.entity.City;
import org.alvis.service.ICityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: CityService相关的测试
 * @author: luchengwen
 * @create: 2020-08-08 17:38
 **/

@SpringBootTest
public class CityServiceTest {

    @Autowired
    private ICityService cityService;

    @Test
    public void findAll() {
        List<City> list = cityService.findAll();
        System.out.println(list);
    }

    @Test
    public void findByCity () {
        List<City> list = cityService.findByCity("北京市");
        System.out.println(list);
    }

    @Test
    public void add() {
        City city = new City("test","test","test","test","test");
        boolean add = cityService.add(city);
        System.out.println(add);
    }
}
