package org.alvis.city.dao;

import org.alvis.dao.ICityDao;
import org.alvis.entity.City;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @program: weather-analysis
 * @description: cityDao相关的测试类
 * @author: luchengwen
 * @create: 2020-08-08 16:49
 **/

@SpringBootTest
public class CityDaoTest {

    @Autowired
    private ICityDao cityDao;

    /**
     * 功能描述 城市的插入测试
     */
    @Test
    public void insertBasicData() {
        cityDao.insertBasicData();
    }

    /**
     * 功能描述 城市的查询
     */
    @Test
    public void queryCityTest() {
        List<City> all = cityDao.findAll();
        System.out.println(all);

    }

    /**
     * 功能描述 城市的查询
     */
    @Test
    public void queryCityByCityTest() {
        List<City> all = cityDao.findByCity("北京市");
        System.out.println(all);

    }
}
